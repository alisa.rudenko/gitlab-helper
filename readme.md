### Prerequisites

1. Downloads the repo's contents
2. Open the folder as project in your IDE
3. Make necessary changes to buttons' names and insert links

### Installation:

1. Open extension management in Chrome (chrome://extensions/)
2. Click on "Load Unpacked" button (загрузить распакованное расширение)
3. Select a folder with the extension's files (the folder should not be archived) and proceed
4. Check that extension was loaded and now displays with other extensions
5. Pin the extension in top browser's bar for easy access

**Note:** you can make changes to the contents **without the need to re-install the extension!**
Just do the needed changes in IDE and they will be applied automatically and immediately - just re-click the icon of the extension in Chrome.

Please do not make any changes to the **manifest.json** file - it can affect the work of the extension in a negative way.

![alt text](image.png)